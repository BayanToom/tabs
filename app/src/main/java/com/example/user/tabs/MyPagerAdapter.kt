package com.example.user.tabs

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? = when (position) {
        0 -> FirstFragment.newInstance()
        1 -> SecondFragment.newInstance()
        2 -> ThirdFragment.newInstance()
        else -> null
    }

   /* override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "TAB 1"
        1 -> "TAB 2"
        2 -> "TAB 3"
        else -> ""
    }*/

    override fun getCount(): Int = 3
}
