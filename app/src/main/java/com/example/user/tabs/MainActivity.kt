package com.example.user.tabs

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        
        val tabLayout: TabLayout = findViewById(R.id.tab_layout)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        val adapter = MyPagerAdapter(supportFragmentManager)

        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

        /* tabLayout.addTab(tabLayout.newTab().setText("Songs"))
         tabLayout.addTab(tabLayout.newTab().setText("Albums"))
         tabLayout.addTab(tabLayout.newTab().setText("Artists"))*/

        tabLayout.getTabAt(0)!!.setIcon(android.R.drawable.ic_lock_idle_alarm)
        tabLayout.getTabAt(1)!!.setIcon(android.R.drawable.ic_dialog_email)
        tabLayout.getTabAt(2)!!.setIcon(android.R.drawable.ic_menu_camera)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }


}
